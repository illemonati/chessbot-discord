__author__ = "illemonati"
__copyright__ = "illemonati"
__license__ = "mit"
# from chessbot_discord import __version__

import os
from discord_client import ChessBotClient


def main():
    client = ChessBotClient()
    client.run(os.getenv('CHESS_BOT_DISCORD_TOKEN'))


if __name__ == '__main__':
    main()
