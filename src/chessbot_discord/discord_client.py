import discord
from games_manager import GamesManager
from game import Game
from datetime import datetime


class ChessBotClient(discord.Client):
    def __init__(self, **options):
        super().__init__(**options)
        self.games_manager = GamesManager()

    async def on_ready(self):
        print('Logged on as', self.user)

    async def handle_game_creation(self, player1, player2, ogmessage):
        new_channel = await ogmessage.guild.create_text_channel(f'{player1}-vs-{player2}-{datetime.now()}')
        await ogmessage.channel.send(f'{player1} is now playing with {player2}\nPlease head to {new_channel.mention}')
        await self.games_manager.handle_game(Game(player1, player2, new_channel, ogmessage.guild, self.user))

    async def on_message(self, message):
        if message.content == 'ping':
            await message.channel.send('pong')

        if message.content.startswith('!move'):
            await self.games_manager.handle_move(message)

        if message.content.startswith('!startchess'):
            # if len(message.mentions) != 1:
            #     await message.channel.send('Please mention exactly 1 opponent!')
            #     return
            if len(message.mentions) != 2:
                await message.channel.send('Please mention exactly 2 players!')
            player1 = message.mentions[0]
            opponent = message.mentions[1]
            await self.handle_game_creation(player1, opponent, message)
