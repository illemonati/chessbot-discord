import chess
import chess.svg
import chess.engine
import discord
import io
import cairosvg
import os


class Game:
    def __init__(self, player1, player2, channel, guild, bot_player):
        self.player1 = player1
        self.player2 = player2
        self.channel = channel
        self.guild = guild
        self.turn = self.player1
        self.bot_player = bot_player
        self.engine = chess.engine.SimpleEngine.popen_uci(
            os.getenv("CHESS_BOT_DISCORD_ENGINE_PATH"))
        self.board = chess.Board()

    async def play_move(self):
        board_svg = chess.svg.board(self.board)
        arr = io.BytesIO()
        cairosvg.svg2png(bytestring=board_svg.encode('utf-8'), write_to=arr)
        arr.seek(0)
        file = discord.File(arr, filename="board.png")
        embed = discord.Embed()
        embed.title = "Board State"
        embed.description = f"Its {self.turn}'s turn to move"
        embed.set_image(url="attachment://board.png")
        await self.channel.send(self.turn.mention, file=file, embed=embed)
        if self.turn == self.bot_player:
            await self.engine_play()

    async def engine_play(self):
        result = self.engine.play(
            self.board, limit=chess.engine.Limit(time=5))
        move = result.move
        await self.channel.send(f'!move {self.board.san(move)}')

    async def handle_move(self, message: discord.Message):
        if message.author not in [self.player1, self.player2]:
            await message.channel.send(f'{message.author.mention} you are not a player in this game')
            return
        if message.author != self.turn:
            await message.channel.send(f'{message.author.mention} its not your turn')
            return
        content_arr = message.content.split(" ")
        if len(content_arr) != 2:
            await message.channel.send(f'{message.author.mention} please only put your move as argument')
            return
        await message.channel.send(f'{self.turn} entered {content_arr[1]}')
        try:
            self.board.push_san(content_arr[1])
        except ValueError as e:
            await message.channel.send(f'{self.turn.mention} that was not a valid move, try again')
            return

        if not self.board.is_game_over():
            if self.turn == self.player1:
                self.turn = self.player2
            else:
                self.turn = self.player1
            await self.play_move()
        else:
            board_svg = chess.svg.board(self.board)
            arr = io.BytesIO()
            cairosvg.svg2png(
                bytestring=board_svg.encode('utf-8'), write_to=arr)
            arr.seek(0)
            file = discord.File(arr, filename="board.png")
            embed = discord.Embed()
            embed.title = "Game Over"
            result = self.board.result()
            if result == '1-0':
                embed.description = f"{self.player1} Wins"
            elif result == '0-1':
                embed.description = f"{self.player2} Wins"
            else:
                embed.description = "Tie"
            embed.set_image(url="attachment://board.png")
            await self.channel.send(file=file, embed=embed)
            await self.channel.set_permissions(self.guild.default_role, send_messages=False)

    async def start(self):
        start_message = f'Starting game between {self.player1} and {self.player2} at {self.channel}'
        await self.channel.send(start_message)
        await self.play_move()
