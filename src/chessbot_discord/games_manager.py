from game import Game


class GamesManager:
    def __init__(self):
        self.games = []

    async def handle_game(self, game: Game):
        self.games.append(game)
        await game.start()

    async def handle_move(self, message):
        for game in self.games:
            if game.channel == message.channel:
                await game.handle_move(message)
                return
        await message.channel.send(f'{message.author.mention} you are not in a valid channel')
